#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint beaconsmind_sdk.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'beaconsmind_sdk'
  s.version          = '1.0.0'
  s.summary          = 'Beaconsmind SDK for Flutter; iOS implementation'
  s.description      = <<-DESC
Beaconsmind SDK for Flutter.
                       DESC
  s.homepage         = 'https://beaconsmind.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Beaconsmind' => 'augustinjuricic@beaconsmind.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.dependency 'Beaconsmind'
  s.platform = :ios, '10.0'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  s.swift_version = '5.0'
end
