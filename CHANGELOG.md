## 1.0.0

* Initial release
* Support Beaconsmind iOS SDK 3.3.0
* Support Beaconsmind Android SDK 1.4.1
