# Beaconsmind SDK

[Beaconsmind](https://beaconsmind.com/) SDK for Flutter.

## Prerequisites

As a Beaconsmind client, a contained environment is provided for Beaconsmind services being hosted and maintained by
Beaconsmind.

The hostname of the provided environment is required to properly access Beaconsmind API and to use Beaconsmind SDK

> example: `https://adidas.bms.beaconsmind.com`

Please contact Beaconsmind support for the required information about acquiring a valid environment and hostname.

## 1. Installing

Add `beaconsmind_sdk` to your app `pubspec.yaml`.

Make sure your *iOS Deployment Target* is 10.0 or above.

Make sure your Android *minSdkVersion* is 23 or above.

## 2. Permissions

### iOS

in Xcode set the following permissions:

- Location updates
- Background fetch
- Remote notifications
- Background processing

### Android

Permissions for *Android* are managed by the SDK

#### Handle device restarted

To keep beacon scanning working even if the device is restarted make sure to add this receiver element to your
`android/app/src/main/AndroidManifest.xml`.

```xml

<receiver android:name="com.beaconsmind.sdk.BootCompleteReceiver"
          android:exported="true">
    <intent-filter>
        <action android:name="android.intent.action.BOOT_COMPLETED"/>
        <category android:name="android.intent.category.DEFAULT"/>
    </intent-filter>
</receiver>
```

### Request permissions from user

```
Beaconsmind.instance.requestPermissions();
```

## 3. Initialize the SDK

This will start the SDK, and try to retrieve the logged-in user.

```
await Beaconsmind.instance.start(
  hostname: 'https://test-develop-suite.azurewebsites.net/',
  appVersion: '1.0.0',
  androidNotificationBadgeName: 'ic_beacons',
  androidNotificationChannelName: 'beaconsmind',
  androidNotificationTitle: 'Beaconsmind sdk demo',
  androidNotificationText: 'Listening to beacons',
);
```

## 4. Authentication

### When using Beaconsmind authentication

This approach implies that the customer data is kept in Beaconsmind backend. Use UserManager.login method to perform
customer authentication.

To login existing user.

```
try {
  final user = await Beaconsmind.instance.login(
    username: _usernameController.text,
    password: _passController.text,
  );
} catch (e) {
  final snackBar = SnackBar(
    content: Text(e.toString()),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
```

To register a new user.

```
try {
  final user = await Beaconsmind.instance.signup(
    username: _emailController.text,
    firstName: _firstNameController.text,
    lastName: _lastNameController.text,
    password: _passController.text,
    confirmPassword: _passConfirmController.text,
  );
  Navigator.of(context).pop();
} catch (e) {
  final snackBar = SnackBar(
    content: Text(e.toString()),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
```

### When using own authentication mechanism

This approach implies that the customer data is kept in clients backend. The authentication is done by the client app
before the Beaconsmind API is used. Whenever customer is authenticated, use `Beaconsmind.instance.importAccount` method
to send basic customer data to Beaconsmind. The customer data is updated only the first time the method is called. The
client app can track if the customer was imported before and omit personal data from following importAccount calls.

## 5. Listening to SDK context changes

The SDk provides a `Stream` for the user login/logout changes. `Beaconsmind.instance.contextEvents()`

```
Beaconsmind.instance.contextEvents().listen((BeaconsmindSdkContext? event) {
    // When event is null, the user is not logged in.
    // When the event is no nill, the user is logged in.
});
```

## 6. Notifications

The SDK uses *APNS* for iOS, and *Firebase* for Android. You will need to use two packages from pub.
[firebase_messaging](https://pub.dev/packages/firebase_messaging)
& [flutter_apns](https://pub.dev/packages/flutter_apns).

### Setting device token

```
final connector = createPushConnector();
connector.token.addListener(tokenListener);

void tokenListener() {
  if (connector.token.value != null) {
    // The value will be APNS on iOS & FCM on Android.
    // Send the device token to Beaconsmind SDK.
    Beaconsmind.instance
        .registerDeviceToken(deviceToken: connector.token.value!);
  }
}
```

## 7. Offers

### Getting offers

- `loadOffer({required int id})`. Will return a single offer.
- `loadOffers()`. Will return a list of currently active offers.

### Interacting with offers

- `markOfferAsReceived({required int offerId})`. Call this when an ofer is received via push.
- `markOfferAsRead({required int offerId})`. Call this when the user open the offer.
- `markOfferAsRedeemed({required int offerId})`. Call this when the user redeems the offer.

## 8. Beacons

### Interacting with beacons

- `startListeningBeacons()`.
- `stopListeningBeacons()`.
- `getBeaconsSummary()`.

### Ream more about beacons

- [iOS setup](https://gitlab.com/beaconsmind/client-sdk-ios/-/blob/main/Documentation/Beacons.md).
- [Android setup](https://gitlab.com/beaconsmind/client-sdk-android/-/blob/main/Documentation/Beacons.md).


## 7. Disposing

Call `Beaconsmind.instance.dispose();` to clean app resources and prevent memory leaks.