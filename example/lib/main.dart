import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';
import 'package:beaconsmind_flutter_demo/app_screen.dart';
import 'package:beaconsmind_flutter_demo/firebase_options.dart';
import 'package:beaconsmind_flutter_demo/login_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (Platform.isAndroid) {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
  }

  runApp(const MyMaterialApp());
}

class MyMaterialApp extends StatelessWidget {
  const MyMaterialApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Beaconsmind',
      home: MyApp(),
    );
  }
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Stream<BeaconsmindSdkContext?>? contextEvents;

  @override
  void initState() {
    super.initState();
    initialize();
  }

  Future<void> initialize() async {
    await Beaconsmind.instance.requestPermissions();
    await Beaconsmind.instance.start(
      hostname: 'https://test-develop-suite.azurewebsites.net/',
      appVersion: '0.0.1',
      androidNotificationChannelName: 'beaconsmind',
      androidNotificationTitle: 'Beaconsmind sdk demo',
      androidNotificationText: 'Listening to beacons',
    );
    contextEvents = Beaconsmind.instance.contextEvents();
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    Beaconsmind.instance.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Beaconsmind example app'),
      ),
      body: StreamBuilder<BeaconsmindSdkContext?>(
        stream: contextEvents,
        builder: (BuildContext context,
            AsyncSnapshot<BeaconsmindSdkContext?> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Error: ${snapshot.error.toString()}'),
                  ElevatedButton(
                    onPressed: () {
                      initialize();
                    },
                    child: const Text('Try again'),
                  ),
                ],
              ),
            );
          } else {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Center(
                  child: ElevatedButton(
                    onPressed: () {
                      initialize();
                    },
                    child: const Text('Initialize SDK'),
                  ),
                );
              case ConnectionState.waiting:
                return const Center(
                  child: Text('Waiting for connection from SDK'),
                );
              case ConnectionState.active:
                if (snapshot.data == null) {
                  return const LoginScreen();
                } else {
                  return const AppScreen();
                }
              case ConnectionState.done:
                return Center(
                  child: ElevatedButton(
                    onPressed: () {
                      initialize();
                    },
                    child: const Text('Initialize SDK again'),
                  ),
                );
            }
          }
        },
      ),
    );
  }
}
