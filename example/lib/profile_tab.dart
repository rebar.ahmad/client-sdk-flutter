import 'package:flutter/material.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';

class ProfileTab extends StatefulWidget {
  const ProfileTab({Key? key}) : super(key: key);

  @override
  State<ProfileTab> createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  Future<Profile?>? _profileResponse;

  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _streetController = TextEditingController();
  final _cityController = TextEditingController();
  final _countryController = TextEditingController();
  final _langController = TextEditingController();
  bool _disablePushNotifications = false;
  DateTime? _birthDate;

  @override
  initState() {
    super.initState();
    initialize();
  }

  void initialize() {
    _profileResponse = Beaconsmind.instance.getProfile()
      ..then((value) {
        if (value != null) {
          setProfileControllers(value);
        }
      });
  }

  void setProfileControllers(Profile profile) {
    _firstNameController.text = profile.firstName;
    _lastNameController.text = profile.lastName;
    _streetController.text = profile.street ?? '';
    _cityController.text = profile.city ?? '';
    _countryController.text = profile.country ?? '';
    _langController.text = profile.language ?? '';
    _disablePushNotifications = profile.disablePushNotifications;
    _birthDate = profile.birthDate;
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _streetController.dispose();
    _cityController.dispose;
    _countryController.dispose();
    _langController.dispose;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Profile?>(
      future: _profileResponse,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Error: ${snapshot.error.toString()}'),
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      initialize();
                    });
                  },
                  child: const Text('Try again'),
                ),
              ],
            ),
          );
        } else {
          return Column(
            children: [
              Expanded(
                child: ListView(
                  padding: const EdgeInsets.symmetric(horizontal: 18),
                  children: [
                    TextFormField(
                      controller: _firstNameController,
                      decoration: const InputDecoration(
                        labelText: 'First Name',
                      ),
                    ),
                    const SizedBox(height: 18),
                    TextFormField(
                      controller: _lastNameController,
                      decoration: const InputDecoration(
                        labelText: 'Last Name',
                      ),
                    ),
                    const SizedBox(height: 18),
                    TextFormField(
                      controller: _streetController,
                      decoration: const InputDecoration(
                        labelText: 'Street',
                      ),
                    ),
                    const SizedBox(height: 18),
                    TextFormField(
                      controller: _cityController,
                      decoration: const InputDecoration(
                        labelText: 'City',
                      ),
                    ),
                    const SizedBox(height: 18),
                    TextFormField(
                      controller: _countryController,
                      decoration: const InputDecoration(
                        labelText: 'Country',
                      ),
                    ),
                    const SizedBox(height: 18),
                    TextFormField(
                      controller: _langController,
                      decoration: const InputDecoration(
                        labelText: 'Language',
                      ),
                    ),
                    const SizedBox(height: 18),
                    ListTile(
                      title: const Text('Date of birth'),
                      subtitle: Text(
                        _birthDate != null
                            ? '${_birthDate!.day}/${_birthDate!.month}/${_birthDate!.year}'
                            : 'Unset',
                      ),
                      trailing: ElevatedButton(
                        onPressed: () async {
                          final res = await showDatePicker(
                            context: context,
                            initialDate: _birthDate != null
                                ? _birthDate!
                                : DateTime.now(),
                            firstDate: DateTime(1900),
                            lastDate: DateTime(2100),
                          );
                          if (res != null) {
                            setState(() {
                              _birthDate = res;
                            });
                          }
                        },
                        child: const Text('Change'),
                      ),
                    ),
                    const SizedBox(height: 18),
                    SwitchListTile(
                      title: const Text('Disable push notifications'),
                      value: _disablePushNotifications,
                      onChanged: (value) {
                        setState(() {
                          _disablePushNotifications = value;
                        });
                      },
                    ),
                    const SizedBox(height: 18),
                    ElevatedButton(
                      onPressed: () async {
                        try {
                          const snackBar = SnackBar(
                            content: Text('Saving'),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          //
                          final res = await Beaconsmind.instance.updateProfile(
                            firstName: _firstNameController.text,
                            lastName: _lastNameController.text,
                            street: _streetController.text,
                            city: _cityController.text,
                            country: _countryController.text,
                            language: _langController.text,
                            birthDate: _birthDate,
                            disablePushNotifications: _disablePushNotifications,
                          );
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                          if (res != null) {
                            const snackBar = SnackBar(
                              content: Text('Profile updated'),
                            );
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                            setProfileControllers(res);
                          }
                        } catch (e) {
                          final snackBar = SnackBar(
                            content: Text('Profile update failed\n$e'),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        }
                      },
                      child: const Text(
                        'UPDATE',
                      ),
                    ),
                    ElevatedButton(
                      onPressed: Beaconsmind.instance.logout,
                      child: const Text(
                        'LOGOUT',
                      ),
                    ),
                  ],
                ),
              ),
            ],
          );
        }
      },
    );
  }
}
