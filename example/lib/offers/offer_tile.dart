import 'package:beaconsmind_flutter_demo/offers/offer_screen.dart';
import 'package:flutter/material.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';

class OfferTile extends StatelessWidget {
  const OfferTile({
    Key? key,
    required this.offer,
  }) : super(key: key);

  final Offer offer;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Card(
        key: Key(offer.offerId.toString()),
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                offer.title,
                style: theme.textTheme.headline5,
              ),
              const SizedBox(height: 8),
              Image.network(offer.imageUrl),
              const SizedBox(height: 8),
              Text(
                offer.text,
                style: theme.textTheme.bodyText1,
              ),
              const SizedBox(height: 8),
              Center(
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => OfferScreen(
                          offerId: offer.offerId,
                          offer: offer,
                        ),
                      ),
                    );
                  },
                  child: const Text('Learn More'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
