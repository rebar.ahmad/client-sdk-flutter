import 'package:flutter/material.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';
import 'package:url_launcher/url_launcher.dart';

class OfferScreen extends StatefulWidget {
  const OfferScreen({
    Key? key,
    required this.offerId,
    this.offer,
  }) : super(key: key);

  final int offerId;
  final Offer? offer;

  @override
  State<OfferScreen> createState() => _OfferScreenState();
}

class _OfferScreenState extends State<OfferScreen> {
  Future<Offer>? _offer;

  @override
  void initState() {
    super.initState();
    getOffer(widget.offer);
  }

  void getOffer(Offer? offer) {
    if (offer != null) {
      _offer = Future.value(offer);
    } else {
      _offer = Beaconsmind.instance.loadOffer(
        id: widget.offerId,
      );
    }
    _offer!.then((value) {
      // Mark the offer as read on Beaconsmind backend.
      Beaconsmind.instance.markOfferAsRead(offerId: value.offerId);
    });
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Offer details'),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          // When user refresh, get the offer from API.
          getOffer(null);
        },
        child: ListView(
          children: [
            FutureBuilder<Offer>(
              future: _offer,
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                }
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    return Center(
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            _offer = Beaconsmind.instance.loadOffer(
                              id: widget.offerId,
                            );
                          });
                        },
                        child: const Text('Get offer'),
                      ),
                    );
                  case ConnectionState.waiting:
                    return const Center(
                      child: Text('Waiting for connection from SDK'),
                    );
                  case ConnectionState.done:
                  case ConnectionState.active:
                    if (snapshot.hasData) {
                      final offer = snapshot.data!;
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Card(
                          key: Key(offer.offerId.toString()),
                          child: Padding(
                            padding: const EdgeInsets.all(16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  offer.title,
                                  style: theme.textTheme.headline5,
                                ),
                                const SizedBox(height: 8),
                                Image.network(offer.imageUrl),
                                const SizedBox(height: 8),
                                Text(
                                  offer.text,
                                  style: theme.textTheme.bodyText1,
                                ),
                                const SizedBox(height: 8),
                                Text(
                                  'Offer validity: ${offer.validity}',
                                  style: theme.textTheme.bodyText1,
                                ),
                                const SizedBox(height: 8),
                                Text.rich(
                                  TextSpan(
                                    text: 'Redeemed:',
                                    children: [
                                      WidgetSpan(
                                        alignment: PlaceholderAlignment.middle,
                                        child: Icon(
                                          offer.isRedeemed
                                              ? Icons.check
                                              : Icons.close,
                                          color: offer.isRedeemed
                                              ? theme.colorScheme.primary
                                              : theme.colorScheme.error,
                                        ),
                                      ),
                                    ],
                                  ),
                                  style: theme.textTheme.bodyText1,
                                ),
                                const SizedBox(height: 8),
                                Text.rich(
                                  TextSpan(
                                    text: 'Voucher:',
                                    children: [
                                      WidgetSpan(
                                        alignment: PlaceholderAlignment.middle,
                                        child: Icon(
                                          offer.isVoucher
                                              ? Icons.check
                                              : Icons.close,
                                          color: offer.isVoucher
                                              ? theme.colorScheme.primary
                                              : theme.colorScheme.error,
                                        ),
                                      ),
                                    ],
                                  ),
                                  style: theme.textTheme.bodyText1,
                                ),
                                const SizedBox(height: 16),
                                Center(
                                  child: ElevatedButton(
                                    onPressed: () {
                                      launch(offer.callToAction.link);
                                    },
                                    child: Text(offer.callToAction.title),
                                  ),
                                ),
                                Center(
                                  child: ElevatedButton(
                                    onPressed: () async {
                                      // Redeem the offer.
                                      Beaconsmind.instance.markOfferAsRedeemed(
                                        offerId: offer.offerId,
                                      );
                                      // refresh the offer from API
                                      getOffer(null);
                                    },
                                    child: const Text('Mark as Redeemed'),
                                  ),
                                ),
                                const SizedBox(height: 8),
                              ],
                            ),
                          ),
                        ),
                      );
                    } else {
                      return Center(
                        child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              _offer = Beaconsmind.instance.loadOffer(
                                id: widget.offerId,
                              );
                            });
                          },
                          child: const Text('Get offers'),
                        ),
                      );
                    }
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
