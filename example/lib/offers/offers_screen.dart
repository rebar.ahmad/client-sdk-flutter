import 'package:flutter/material.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';
import 'package:beaconsmind_flutter_demo/offers/offer_tile.dart';

class OffersScreen extends StatefulWidget {
  const OffersScreen({Key? key}) : super(key: key);

  @override
  State<OffersScreen> createState() => _OffersScreenState();
}

class _OffersScreenState extends State<OffersScreen> {
  Future<List<Offer>>? _offersFuture;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Offers'),
      ),
      body: FutureBuilder<List<Offer>>(
        future: _offersFuture,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      _offersFuture = Beaconsmind.instance.loadOffers();
                    });
                  },
                  child: const Text('Get offers'),
                ),
              );
            case ConnectionState.waiting:
              return const Center(
                child: Text('Waiting for connection from SDK'),
              );
            case ConnectionState.done:
            case ConnectionState.active:
              if (snapshot.hasData) {
                final offers = snapshot.data!;

                return RefreshIndicator(
                  onRefresh: () async {
                    _offersFuture = Beaconsmind.instance.loadOffers();
                  },
                  child: ListView.builder(
                    itemCount: offers.length,
                    itemBuilder: (context, index) => OfferTile(
                      offer: offers[index],
                    ),
                  ),
                );
              } else {
                return Center(
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _offersFuture = Beaconsmind.instance.loadOffers();
                      });
                    },
                    child: const Text('Get offers'),
                  ),
                );
              }
          }
        },
      ),
    );
  }
}
