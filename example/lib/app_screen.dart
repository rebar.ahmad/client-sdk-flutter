import 'dart:async';

import 'package:beaconsmind_flutter_demo/offers/offer_screen.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';
import 'package:flutter/material.dart';
import 'package:beaconsmind_flutter_demo/beacons/beacons_screen.dart';
import 'package:beaconsmind_flutter_demo/offers/offers_screen.dart';
import 'package:beaconsmind_flutter_demo/profile_tab.dart';
import 'package:flutter_apns/flutter_apns.dart';

Future<void> _onBackgroundMessage(RemoteMessage message) async {
  try {
    Beaconsmind.instance.markOfferAsReceived(
      offerId: Beaconsmind.instance.parseOfferId(data: message.data)!,
    );
  } catch (e) {
    debugPrint(e.toString());
    // This push is not from Beaconsmind
  }
}

class AppScreen extends StatefulWidget {
  const AppScreen({Key? key}) : super(key: key);

  @override
  State<AppScreen> createState() => _AppScreenState();
}

class _AppScreenState extends State<AppScreen> {
  var _selectedIndex = 1;

  // Beaconsmind SDK setup
  late final notificationsConnector = createPushConnector();
  late final StreamSubscription<BeaconsmindSdkContext?> beaconsmindContextSub;
  Future<void> setupNotifications() async {
    notificationsConnector.configure(
      onLaunch: _onMessage,
      onResume: _onMessage,
      onMessage: _onMessage,
      onBackgroundMessage: _onBackgroundMessage,
    );
    setDeviceToken();
    notificationsConnector.token.addListener(setDeviceToken);
  }

  void setDeviceToken() async {
    if (notificationsConnector.token.value != null &&
        Beaconsmind.instance.currentContext != null) {
      await Beaconsmind.instance.registerDeviceToken(
        deviceToken: notificationsConnector.token.value!,
      );
    }
  }

  Future<void> _onMessage(RemoteMessage message) async {
    try {
      final offerId = Beaconsmind.instance.parseOfferId(data: message.data)!;
      Beaconsmind.instance.markOfferAsReceived(offerId: offerId);
      // Take teh user to the offer screen automagically!
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => OfferScreen(
            offerId: offerId,
            offer: null,
          ),
        ),
      );
    } catch (e) {
      debugPrint(e.toString());
      // This push is not from Beaconsmind
    }
  }

  @override
  void initState() {
    super.initState();
    beaconsmindContextSub =
        Beaconsmind.instance.contextEvents().listen((event) {
      if (event != null) {
        // user is logged in
        setupNotifications();
      } else {
        // user logged out
        Beaconsmind.instance.stopListeningBeacons();
        notificationsConnector.unregister();
      }
    });
  }

  @override
  void dispose() {
    beaconsmindContextSub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (context) {
          switch (_selectedIndex) {
            case 0:
              return const BeaconsScreen();
            case 1:
              return const OffersScreen();
            case 2:
              return const ProfileTab();
            default:
              return const Text('App');
          }
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.bluetooth),
            label: 'Beacons',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_offer),
            label: 'Offers',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
