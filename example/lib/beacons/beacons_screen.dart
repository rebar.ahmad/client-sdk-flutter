import 'package:flutter/material.dart';
import 'package:beaconsmind_sdk/beaconsmind_sdk.dart';

class BeaconsScreen extends StatefulWidget {
  const BeaconsScreen({Key? key}) : super(key: key);

  @override
  State<BeaconsScreen> createState() => _BeaconsScreenState();
}

class _BeaconsScreenState extends State<BeaconsScreen> {
  Future<List<Beacon>>? _beaconsFuture;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Beacons'),
      ),
      body: FutureBuilder<List<Beacon>>(
        future: _beaconsFuture,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      _beaconsFuture = Beaconsmind.instance.getBeaconsSummary();
                    });
                  },
                  child: const Text('Get beacons'),
                ),
              );
            case ConnectionState.waiting:
              return const Center(
                child: Text('Waiting for connection from SDK'),
              );
            case ConnectionState.done:
            case ConnectionState.active:
              if (snapshot.hasData) {
                final beacons = snapshot.data!;

                return ListView.builder(
                  itemCount: beacons.length,
                  itemBuilder: (context, index) {
                    final b = beacons[index];
                    return ListTile(
                      isThreeLine: true,
                      title: Text(b.uuid),
                      subtitle: Text(
                        'Major: ${b.major}'
                        '\n'
                        'Minor: ${b.minor}',
                      ),
                    );
                  },
                );
              } else {
                return Center(
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        _beaconsFuture =
                            Beaconsmind.instance.getBeaconsSummary();
                      });
                    },
                    child: const Text('Get beacons'),
                  ),
                );
              }
          }
        },
      ),
    );
  }
}
