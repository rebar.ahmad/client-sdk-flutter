class BeaconsmindSdkContext {
  final String userId;

  BeaconsmindSdkContext({
    required this.userId,
  });

  factory BeaconsmindSdkContext.fromJson(Map<String, dynamic> json) =>
      BeaconsmindSdkContext(
        userId: json['userId'],
      );

  @override
  bool operator ==(Object other) {
    return other is BeaconsmindSdkContext && other.userId == userId;
  }

  @override
  int get hashCode => runtimeType.hashCode ^ userId.hashCode;
}
