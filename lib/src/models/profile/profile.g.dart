// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Profile _$$_ProfileFromJson(Map<String, dynamic> json) => _$_Profile(
      disablePushNotifications: json['disablePushNotifications'] as bool,
      firstName: json['firstName'] as String,
      joinDate: DateTime.parse(json['joinDate'] as String),
      lastName: json['lastName'] as String,
      newsLetterSubscription: json['newsLetterSubscription'] as bool,
      birthDate: json['birthDate'] == null
          ? null
          : DateTime.parse(json['birthDate'] as String),
      city: json['city'] as String?,
      claims:
          (json['claims'] as List<dynamic>?)?.map((e) => e as String).toList(),
      clubId: json['clubId'] as String?,
      country: json['country'] as String?,
      favoriteStore: json['favoriteStore'] as String?,
      favoriteStoreId: json['favoriteStoreId'] as int?,
      fullName: json['fullName'] as String?,
      gender: json['gender'] as String?,
      houseNumber: json['houseNumber'] as String?,
      id: json['id'] as String?,
      landlinePhone: json['landlinePhone'] as String?,
      language: json['language'] as String?,
      phoneNumber: json['phoneNumber'] as String?,
      roles:
          (json['roles'] as List<dynamic>?)?.map((e) => e as String).toList(),
      street: json['street'] as String?,
      url: json['url'] as String?,
      userName: json['userName'] as String?,
      zipCode: json['zipCode'] as String?,
    );

Map<String, dynamic> _$$_ProfileToJson(_$_Profile instance) =>
    <String, dynamic>{
      'disablePushNotifications': instance.disablePushNotifications,
      'firstName': instance.firstName,
      'joinDate': instance.joinDate.toIso8601String(),
      'lastName': instance.lastName,
      'newsLetterSubscription': instance.newsLetterSubscription,
      'birthDate': instance.birthDate?.toIso8601String(),
      'city': instance.city,
      'claims': instance.claims,
      'clubId': instance.clubId,
      'country': instance.country,
      'favoriteStore': instance.favoriteStore,
      'favoriteStoreId': instance.favoriteStoreId,
      'fullName': instance.fullName,
      'gender': instance.gender,
      'houseNumber': instance.houseNumber,
      'id': instance.id,
      'landlinePhone': instance.landlinePhone,
      'language': instance.language,
      'phoneNumber': instance.phoneNumber,
      'roles': instance.roles,
      'street': instance.street,
      'url': instance.url,
      'userName': instance.userName,
      'zipCode': instance.zipCode,
    };
