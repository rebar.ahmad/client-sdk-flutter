// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'offer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Offer _$$_OfferFromJson(Map<String, dynamic> json) => _$_Offer(
      offerId: json['offerId'] as int,
      title: json['title'] as String,
      text: json['text'] as String,
      imageUrl: json['imageUrl'] as String,
      thumbnailUrl: json['thumbnailUrl'] as String,
      validity: json['validity'] as String,
      isRedeemed: json['isRedeemed'] as bool,
      isVoucher: json['isVoucher'] as bool,
      tileAmount: json['tileAmount'] as int,
      callToAction:
          ButtonModel.fromJson(json['callToAction'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_OfferToJson(_$_Offer instance) => <String, dynamic>{
      'offerId': instance.offerId,
      'title': instance.title,
      'text': instance.text,
      'imageUrl': instance.imageUrl,
      'thumbnailUrl': instance.thumbnailUrl,
      'validity': instance.validity,
      'isRedeemed': instance.isRedeemed,
      'isVoucher': instance.isVoucher,
      'tileAmount': instance.tileAmount,
      'callToAction': instance.callToAction.toJson(),
    };
