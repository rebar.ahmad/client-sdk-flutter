import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:beaconsmind_sdk/src/models/button_model/button_model.dart';

part 'offer.freezed.dart';

part 'offer.g.dart';

@freezed
class Offer with _$Offer {
  const factory Offer({
    required int offerId,
    required String title,
    required String text,
    required String imageUrl,
    required String thumbnailUrl,
    required String validity,
    required bool isRedeemed,
    required bool isVoucher,
    required int tileAmount,
    required ButtonModel callToAction,
  }) = _Offer;

  factory Offer.fromJson(Map<String, dynamic> json) => _$OfferFromJson(json);
}
