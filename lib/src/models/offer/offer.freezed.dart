// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'offer.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Offer _$OfferFromJson(Map<String, dynamic> json) {
  return _Offer.fromJson(json);
}

/// @nodoc
class _$OfferTearOff {
  const _$OfferTearOff();

  _Offer call(
      {required int offerId,
      required String title,
      required String text,
      required String imageUrl,
      required String thumbnailUrl,
      required String validity,
      required bool isRedeemed,
      required bool isVoucher,
      required int tileAmount,
      required ButtonModel callToAction}) {
    return _Offer(
      offerId: offerId,
      title: title,
      text: text,
      imageUrl: imageUrl,
      thumbnailUrl: thumbnailUrl,
      validity: validity,
      isRedeemed: isRedeemed,
      isVoucher: isVoucher,
      tileAmount: tileAmount,
      callToAction: callToAction,
    );
  }

  Offer fromJson(Map<String, Object?> json) {
    return Offer.fromJson(json);
  }
}

/// @nodoc
const $Offer = _$OfferTearOff();

/// @nodoc
mixin _$Offer {
  int get offerId => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  String get text => throw _privateConstructorUsedError;
  String get imageUrl => throw _privateConstructorUsedError;
  String get thumbnailUrl => throw _privateConstructorUsedError;
  String get validity => throw _privateConstructorUsedError;
  bool get isRedeemed => throw _privateConstructorUsedError;
  bool get isVoucher => throw _privateConstructorUsedError;
  int get tileAmount => throw _privateConstructorUsedError;
  ButtonModel get callToAction => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $OfferCopyWith<Offer> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OfferCopyWith<$Res> {
  factory $OfferCopyWith(Offer value, $Res Function(Offer) then) =
      _$OfferCopyWithImpl<$Res>;
  $Res call(
      {int offerId,
      String title,
      String text,
      String imageUrl,
      String thumbnailUrl,
      String validity,
      bool isRedeemed,
      bool isVoucher,
      int tileAmount,
      ButtonModel callToAction});

  $ButtonModelCopyWith<$Res> get callToAction;
}

/// @nodoc
class _$OfferCopyWithImpl<$Res> implements $OfferCopyWith<$Res> {
  _$OfferCopyWithImpl(this._value, this._then);

  final Offer _value;
  // ignore: unused_field
  final $Res Function(Offer) _then;

  @override
  $Res call({
    Object? offerId = freezed,
    Object? title = freezed,
    Object? text = freezed,
    Object? imageUrl = freezed,
    Object? thumbnailUrl = freezed,
    Object? validity = freezed,
    Object? isRedeemed = freezed,
    Object? isVoucher = freezed,
    Object? tileAmount = freezed,
    Object? callToAction = freezed,
  }) {
    return _then(_value.copyWith(
      offerId: offerId == freezed
          ? _value.offerId
          : offerId // ignore: cast_nullable_to_non_nullable
              as int,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
      imageUrl: imageUrl == freezed
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String,
      thumbnailUrl: thumbnailUrl == freezed
          ? _value.thumbnailUrl
          : thumbnailUrl // ignore: cast_nullable_to_non_nullable
              as String,
      validity: validity == freezed
          ? _value.validity
          : validity // ignore: cast_nullable_to_non_nullable
              as String,
      isRedeemed: isRedeemed == freezed
          ? _value.isRedeemed
          : isRedeemed // ignore: cast_nullable_to_non_nullable
              as bool,
      isVoucher: isVoucher == freezed
          ? _value.isVoucher
          : isVoucher // ignore: cast_nullable_to_non_nullable
              as bool,
      tileAmount: tileAmount == freezed
          ? _value.tileAmount
          : tileAmount // ignore: cast_nullable_to_non_nullable
              as int,
      callToAction: callToAction == freezed
          ? _value.callToAction
          : callToAction // ignore: cast_nullable_to_non_nullable
              as ButtonModel,
    ));
  }

  @override
  $ButtonModelCopyWith<$Res> get callToAction {
    return $ButtonModelCopyWith<$Res>(_value.callToAction, (value) {
      return _then(_value.copyWith(callToAction: value));
    });
  }
}

/// @nodoc
abstract class _$OfferCopyWith<$Res> implements $OfferCopyWith<$Res> {
  factory _$OfferCopyWith(_Offer value, $Res Function(_Offer) then) =
      __$OfferCopyWithImpl<$Res>;
  @override
  $Res call(
      {int offerId,
      String title,
      String text,
      String imageUrl,
      String thumbnailUrl,
      String validity,
      bool isRedeemed,
      bool isVoucher,
      int tileAmount,
      ButtonModel callToAction});

  @override
  $ButtonModelCopyWith<$Res> get callToAction;
}

/// @nodoc
class __$OfferCopyWithImpl<$Res> extends _$OfferCopyWithImpl<$Res>
    implements _$OfferCopyWith<$Res> {
  __$OfferCopyWithImpl(_Offer _value, $Res Function(_Offer) _then)
      : super(_value, (v) => _then(v as _Offer));

  @override
  _Offer get _value => super._value as _Offer;

  @override
  $Res call({
    Object? offerId = freezed,
    Object? title = freezed,
    Object? text = freezed,
    Object? imageUrl = freezed,
    Object? thumbnailUrl = freezed,
    Object? validity = freezed,
    Object? isRedeemed = freezed,
    Object? isVoucher = freezed,
    Object? tileAmount = freezed,
    Object? callToAction = freezed,
  }) {
    return _then(_Offer(
      offerId: offerId == freezed
          ? _value.offerId
          : offerId // ignore: cast_nullable_to_non_nullable
              as int,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
      imageUrl: imageUrl == freezed
          ? _value.imageUrl
          : imageUrl // ignore: cast_nullable_to_non_nullable
              as String,
      thumbnailUrl: thumbnailUrl == freezed
          ? _value.thumbnailUrl
          : thumbnailUrl // ignore: cast_nullable_to_non_nullable
              as String,
      validity: validity == freezed
          ? _value.validity
          : validity // ignore: cast_nullable_to_non_nullable
              as String,
      isRedeemed: isRedeemed == freezed
          ? _value.isRedeemed
          : isRedeemed // ignore: cast_nullable_to_non_nullable
              as bool,
      isVoucher: isVoucher == freezed
          ? _value.isVoucher
          : isVoucher // ignore: cast_nullable_to_non_nullable
              as bool,
      tileAmount: tileAmount == freezed
          ? _value.tileAmount
          : tileAmount // ignore: cast_nullable_to_non_nullable
              as int,
      callToAction: callToAction == freezed
          ? _value.callToAction
          : callToAction // ignore: cast_nullable_to_non_nullable
              as ButtonModel,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Offer implements _Offer {
  const _$_Offer(
      {required this.offerId,
      required this.title,
      required this.text,
      required this.imageUrl,
      required this.thumbnailUrl,
      required this.validity,
      required this.isRedeemed,
      required this.isVoucher,
      required this.tileAmount,
      required this.callToAction});

  factory _$_Offer.fromJson(Map<String, dynamic> json) =>
      _$$_OfferFromJson(json);

  @override
  final int offerId;
  @override
  final String title;
  @override
  final String text;
  @override
  final String imageUrl;
  @override
  final String thumbnailUrl;
  @override
  final String validity;
  @override
  final bool isRedeemed;
  @override
  final bool isVoucher;
  @override
  final int tileAmount;
  @override
  final ButtonModel callToAction;

  @override
  String toString() {
    return 'Offer(offerId: $offerId, title: $title, text: $text, imageUrl: $imageUrl, thumbnailUrl: $thumbnailUrl, validity: $validity, isRedeemed: $isRedeemed, isVoucher: $isVoucher, tileAmount: $tileAmount, callToAction: $callToAction)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Offer &&
            const DeepCollectionEquality().equals(other.offerId, offerId) &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.text, text) &&
            const DeepCollectionEquality().equals(other.imageUrl, imageUrl) &&
            const DeepCollectionEquality()
                .equals(other.thumbnailUrl, thumbnailUrl) &&
            const DeepCollectionEquality().equals(other.validity, validity) &&
            const DeepCollectionEquality()
                .equals(other.isRedeemed, isRedeemed) &&
            const DeepCollectionEquality().equals(other.isVoucher, isVoucher) &&
            const DeepCollectionEquality()
                .equals(other.tileAmount, tileAmount) &&
            const DeepCollectionEquality()
                .equals(other.callToAction, callToAction));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(offerId),
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(text),
      const DeepCollectionEquality().hash(imageUrl),
      const DeepCollectionEquality().hash(thumbnailUrl),
      const DeepCollectionEquality().hash(validity),
      const DeepCollectionEquality().hash(isRedeemed),
      const DeepCollectionEquality().hash(isVoucher),
      const DeepCollectionEquality().hash(tileAmount),
      const DeepCollectionEquality().hash(callToAction));

  @JsonKey(ignore: true)
  @override
  _$OfferCopyWith<_Offer> get copyWith =>
      __$OfferCopyWithImpl<_Offer>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_OfferToJson(this);
  }
}

abstract class _Offer implements Offer {
  const factory _Offer(
      {required int offerId,
      required String title,
      required String text,
      required String imageUrl,
      required String thumbnailUrl,
      required String validity,
      required bool isRedeemed,
      required bool isVoucher,
      required int tileAmount,
      required ButtonModel callToAction}) = _$_Offer;

  factory _Offer.fromJson(Map<String, dynamic> json) = _$_Offer.fromJson;

  @override
  int get offerId;
  @override
  String get title;
  @override
  String get text;
  @override
  String get imageUrl;
  @override
  String get thumbnailUrl;
  @override
  String get validity;
  @override
  bool get isRedeemed;
  @override
  bool get isVoucher;
  @override
  int get tileAmount;
  @override
  ButtonModel get callToAction;
  @override
  @JsonKey(ignore: true)
  _$OfferCopyWith<_Offer> get copyWith => throw _privateConstructorUsedError;
}
