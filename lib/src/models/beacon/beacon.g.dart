// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'beacon.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Beacon _$$_BeaconFromJson(Map<String, dynamic> json) => _$_Beacon(
      uuid: json['uuid'] as String,
      major: json['major'] as String,
      minor: json['minor'] as String,
      name: json['name'] as String,
      store: json['store'] as String,
    );

Map<String, dynamic> _$$_BeaconToJson(_$_Beacon instance) => <String, dynamic>{
      'uuid': instance.uuid,
      'major': instance.major,
      'minor': instance.minor,
      'name': instance.name,
      'store': instance.store,
    };
