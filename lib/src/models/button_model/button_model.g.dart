// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'button_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ButtonModel _$$_ButtonModelFromJson(Map<String, dynamic> json) =>
    _$_ButtonModel(
      title: json['title'] as String,
      link: json['link'] as String,
    );

Map<String, dynamic> _$$_ButtonModelToJson(_$_ButtonModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'link': instance.link,
    };
