import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:beaconsmind_sdk/src/models/beacon/beacon.dart';
import 'package:beaconsmind_sdk/src/models/offer/offer.dart';
import 'package:beaconsmind_sdk/src/models/profile/profile.dart';
import 'package:beaconsmind_sdk/src/sdk_context.dart';
import 'package:rxdart/rxdart.dart';

class Beaconsmind {
  static const MethodChannel _channel = MethodChannel('beaconsmind/methods');
  static const EventChannel _contextEvents =
      EventChannel('beaconsmind/contextEvents');

  Beaconsmind();

  static Beaconsmind? _instance;

  static Beaconsmind get instance {
    _instance ??= Beaconsmind();
    return _instance!;
  }

  BehaviorSubject<BeaconsmindSdkContext?>? _contextEventsController;

  //
  // Start
  //
  Future<void> start({
    /// App version.
    required String appVersion,

    /// Host name, example: https://adidas.bms.beaconsmind.com
    required String hostname,

    /// Android notification icon drawable name
    String? androidNotificationBadgeName,

    /// Android notification title
    String? androidNotificationTitle,

    /// Android notification body
    String? androidNotificationText,

    /// Android notification channel name
    String? androidNotificationChannelName,
  }) async {
    await _channel.invokeMethod<bool>('start', {
      'appVersion': appVersion,
      'hostname': hostname,
      'notificationBadgeName': androidNotificationBadgeName,
      'notificationTitle': androidNotificationTitle,
      'notificationText': androidNotificationText,
      'notificationChannelName': androidNotificationChannelName,
    });
  }

  //
  // Authentication
  //
  Future<BeaconsmindSdkContext> signup({
    required String username,
    required String firstName,
    required String lastName,
    required String password,
    required String confirmPassword,
    String? language,
    String? gender,
    int? favoriteStoreID,
    DateTime? birthDate,
  }) async {
    final res = await _channel.invokeMethod<Map>('signup', {
      'username': username,
      'firstName': firstName,
      'lastName': lastName,
      'password': password,
      'confirmPassword': confirmPassword,
      'language': language,
      'gender': gender,
      'favoriteStoreID': favoriteStoreID,
      'birthDateSeconds': birthDate != null
          ? (birthDate.millisecondsSinceEpoch / 1000).toDouble()
          : null,
    });
    if (res != null) {
      final json = Map<String, dynamic>.from(res);
      return BeaconsmindSdkContext.fromJson(json);
    } else {
      return Future.error('signup failed!');
    }
  }

  Future<BeaconsmindSdkContext> login({
    required String username,
    required String password,
  }) async {
    final res = await _channel.invokeMethod<Map?>('login', {
      'username': username,
      'password': password,
    });
    if (res != null) {
      final json = Map<String, dynamic>.from(res);
      return BeaconsmindSdkContext.fromJson(json);
    } else {
      return Future.error('login failed!');
    }
  }

  Future<BeaconsmindSdkContext> importAccount({
    required String id,
    required String email,
    String? firstName,
    String? lastName,
    DateTime? birthDate,
    String? language,
    String? gender,
  }) async {
    final res = await _channel.invokeMethod<Map<String, dynamic>>(
      'importAccount',
      {
        'id': id,
        'email': email,
        'firstName': firstName,
        'lastName': lastName,
        'birthDateSeconds': birthDate != null
            ? (birthDate.millisecondsSinceEpoch / 1000).toDouble()
            : null,
        'language': language,
        'gender': gender,
      },
    );
    return BeaconsmindSdkContext.fromJson(res!);
  }

  Future<Profile?> getProfile() async {
    final res = await _channel.invokeMethod<Map?>('getProfile');
    if (res != null) {
      final json = Map<String, dynamic>.from(res);
      return Profile.fromJson(json);
    }
    return null;
  }

  Future<Profile?> updateProfile({
    required String firstName,
    required String lastName,
    DateTime? birthDate,
    String? city,
    String? country,
    bool? disablePushNotifications,
    int? favoriteStoreID,
    String? gender,
    String? houseNumber,
    String? landlinePhone,
    String? language,
    String? phoneNumber,
    String? street,
    String? zipCode,
  }) async {
    final res = await _channel.invokeMethod<Map?>('updateProfile', {
      'firstName': firstName,
      'lastName': lastName,
      'birthDateSeconds': birthDate != null
          ? (birthDate.millisecondsSinceEpoch / 1000).toDouble()
          : null,
      'city': city,
      'country': country,
      'disablePushNotifications': disablePushNotifications,
      'favoriteStoreID': favoriteStoreID,
      'gender': gender,
      'houseNumber': houseNumber,
      'landlinePhone': landlinePhone,
      'language': language,
      'phoneNumber': phoneNumber,
      'street': street,
      'zipCode': zipCode,
    });
    if (res != null) {
      final json = Map<String, dynamic>.from(res);
      return Profile.fromJson(json);
    } else {
      return null;
    }
  }

  //
  // Context helpers
  //
  Future<void> logout() async {
    await _channel.invokeMethod('logout');
  }

  Future<BeaconsmindSdkContext?> getOAuthContext() async {
    final res =
        await _channel.invokeMethod<Map<String, dynamic>>('getOAuthContext');
    if (res != null) {
      return BeaconsmindSdkContext.fromJson(res);
    }
    return null;
  }

  Future<void> updateHostname({
    required String hostname,
  }) async {
    await _channel.invokeMethod('updateHostname', {
      'hostname': hostname,
    });
  }

  //
  // Beacons
  //
  Future<void> startListeningBeacons() async {
    await _channel.invokeMethod("startListeningBeacons");
  }

  Future<void> stopListeningBeacons() async {
    await _channel.invokeMethod("stopListeningBeacons");
  }

  Future<List<Beacon>> getBeaconsSummary() async {
    final res = await await _channel.invokeMethod('getBeaconsSummary');
    final List<dynamic> json = jsonDecode(res);
    return json.map((e) => Beacon.fromJson(e)).toList();
  }

  //
  // Permissions
  //
  Future<void> requestPermissions() async {
    await _channel.invokeMethod('requestPermissions');
  }

  /// Register the push notifications token with Beaconsmind sdk.
  /// String? token = await FirebaseMessaging.instance.getToken();
  /// register(token: token);
  Future<void> registerDeviceToken({
    required String deviceToken,
  }) async {
    await _channel.invokeMethod('registerDeviceToken', {
      'deviceToken': deviceToken,
    });
  }

  //
  // Offers
  //

  /// Return the offerId from push notification data.
  int? parseOfferId({required Map data}) {
    try {
      return int.parse('${data["offerId"]}');
    } catch (e) {
      // Offer not found
    }
    return null;
  }

  Future<void> markOfferAsReceived({required int offerId}) async {
    await _channel.invokeMethod('markOfferAsReceived', {
      'offerId': offerId,
    });
  }

  Future<void> markOfferAsRead({required int offerId}) async {
    await _channel.invokeMethod('markOfferAsRead', {
      'offerId': offerId,
    });
  }

  Future<void> markOfferAsRedeemed({required int offerId}) async {
    await _channel.invokeMethod('markOfferAsRedeemed', {
      'offerId': offerId,
    });
  }

  Future<Offer> loadOffer({required int id}) async {
    final res = await await _channel.invokeMethod('loadOffer', {
      'offerId': id,
    });
    final Map<dynamic, dynamic> json = jsonDecode(res);
    return Offer.fromJson(json.cast<String, dynamic>());
  }

  Future<List<Offer>> loadOffers() async {
    final res = await await _channel.invokeMethod('loadOffers');
    final List<dynamic> json = jsonDecode(res);
    return json.map((e) => Offer.fromJson(e)).toList();
  }

  //
  // context events stream
  //
  Stream<BeaconsmindSdkContext?> contextEvents() async* {
    if (_contextEventsController == null) {
      _contextEventsController = BehaviorSubject<BeaconsmindSdkContext?>();
      _contextEvents.receiveBroadcastStream().listen((event) {
        if (_contextEventsController?.isClosed == true) {
          return;
        }
        if (event != null) {
          final json = Map<String, dynamic>.from(event);
          try {
            _contextEventsController?.add(BeaconsmindSdkContext.fromJson(json));
          } catch (e, s) {
            _contextEventsController?.addError(e, s);
          }
        } else {
          _contextEventsController?.add(null);
        }
      });
    }
    yield* _contextEventsController!.stream;
  }

  BeaconsmindSdkContext? get currentContext => _contextEventsController?.value;

  /// Call dispose when closing the app, like in a top level dispose function.
  void dispose() {
    if (_contextEventsController?.isClosed != true) {
      _contextEventsController!.close();
    }
    _contextEventsController = null;
    _instance = null;
  }
}
