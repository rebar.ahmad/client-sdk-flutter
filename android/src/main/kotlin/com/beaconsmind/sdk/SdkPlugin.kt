package com.beaconsmind.sdk

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import androidx.annotation.NonNull
import com.beaconsmind.api.models.*
import com.google.gson.Gson
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import java.sql.Date
import java.sql.Timestamp
import java.text.DateFormat
import java.text.SimpleDateFormat

/** SdkPlugin */
class SdkPlugin : FlutterPlugin, ActivityAware, MethodCallHandler, EventChannel.StreamHandler {
    private var mActivity: Activity? = null
    private var mApplication: Application? = null
    private lateinit var methods: MethodChannel
    private lateinit var contextEvents: EventChannel
    private var eventsSink: EventChannel.EventSink? = null


    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        methods = MethodChannel(flutterPluginBinding.binaryMessenger, "beaconsmind/methods")
        contextEvents =
            EventChannel(flutterPluginBinding.binaryMessenger, "beaconsmind/contextEvents")
        methods.setMethodCallHandler(this)
        contextEvents.setStreamHandler(this)
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "start" -> {
                try {
                    val appVersion = call.argument<String>("appVersion")
                    val hostname = call.argument<String>("hostname")
                    val notificationBadgeName = call.argument<String?>("notificationBadgeName")
                    val notificationTitle = call.argument<String?>("notificationTitle")
                    val notificationText = call.argument<String?>("notificationText")
                    val notificationChannelName = call.argument<String?>("notificationChannelName")

                    var notificationBadge: Int? = null
                    if (notificationBadgeName != null && mApplication != null) {
                        notificationBadge =
                            mApplication!!.applicationContext.resources.getIdentifier(
                                notificationBadgeName,
                                "drawable",
                                mApplication!!.applicationContext.packageName
                            )
                    }
                    val config = BeaconsmindConfig.Builder(hostname)
                        .setAppVersion(appVersion)
                        .setNotificationBadge(notificationBadge ?: R.drawable.ic_beacons)
                        .setNotificationTitle(notificationTitle ?: "Beaconsmind")
                        .setNotificationText(notificationText ?: "Listening for Beacons")
                        .setNotificationChannelName(
                            notificationChannelName ?: "beaconsmind"
                        )
                        .build()
                    BeaconsmindSdk.initialize(mApplication, config)
                    result.success(true)
                } catch (e: Exception) {
                    result.error("500", "start failed!", e.toString())
                }
            }
            "signup" -> {
                try {
                    val userName = call.argument<String>("username")!!
                    val firstName = call.argument<String>("firstName")!!
                    val lastName = call.argument<String>("lastName")!!
                    val password = call.argument<String>("password")!!
                    val confirmPassword = call.argument<String>("confirmPassword")!!
                    val language = call.argument<String?>("language")
                    val gender = call.argument<String?>("gender")
                    val favoriteStoreID = call.argument<Int?>("favoriteStoreID")
                    val birthDateSeconds = call.argument<Double?>("birthDateSeconds")
                    var birthday: Date? = null
                    if (birthDateSeconds != null) {
                        val stamp = Timestamp(birthDateSeconds.toLong() * 1000)
                        birthday = Date(stamp.time)
                    }
                    val request = CreateUserRequest()
                    request.username = userName
                    request.firstName = firstName
                    request.lastName = lastName
                    request.password = password
                    request.confirmPassword = confirmPassword
                    request.language = language
                    request.gender = gender
                    request.favoriteStoreId = favoriteStoreID
                    request.birthday = birthday

                    UserManager.getInstance().createAccount(request, object : OnCompleteListener {
                        override fun onSuccess() {
                            val userId = checkUser()
                            if (userId != null) {
                                result.success(mapOf("userId" to userId))
                            } else {
                                result.success(null)
                            }
                        }

                        override fun onError(error: Error) {
                            result.error("500", "signup failed!", error.toString())
                        }
                    }
                    )
                } catch (e: Exception) {
                    result.error("500", "signup failed!", e.toString())
                }
            }
            "login" -> {
                try {
                    val userName = call.argument<String>("username")!!
                    val password = call.argument<String>("password")!!

                    UserManager.getInstance()
                        .login(userName, password, object : OnCompleteListener {
                            override fun onSuccess() {
                                val userId = checkUser()
                                if (userId != null) {
                                    result.success(mapOf("userId" to userId))
                                } else {
                                    result.success(null)
                                }
                            }

                            override fun onError(error: Error) {
                                result.error("500", "login failed!", error.toString())
                            }
                        }
                        )
                } catch (e: Exception) {
                    result.error("500", "login failed!", e.toString())
                }
            }
            "importAccount" -> {
                try {
                    val id = call.argument<String>("id")!!
                    val email = call.argument<String>("email")!!
                    val firstName = call.argument<String>("firstName")
                    val lastName = call.argument<String>("lastName")
                    val language = call.argument<String?>("language")
                    val gender = call.argument<String?>("gender")
                    val birthDateSeconds = call.argument<Double?>("birthDateSeconds")
                    var birthday: Date? = null
                    if (birthDateSeconds != null) {
                        val stamp = Timestamp(birthDateSeconds.toLong() * 1000)
                        birthday = Date(stamp.time)
                    }
                    val request = ImportUserRequest()
                    request.id = id
                    request.email = email
                    request.firstName = firstName
                    request.lastName = lastName
                    request.language = language
                    request.gender = gender
                    request.birthDate = birthday

                    UserManager.getInstance().importAccount(request, object : OnCompleteListener {
                        override fun onSuccess() {
                            val userId = checkUser()
                            if (userId != null) {
                                result.success(mapOf("userId" to userId))
                            } else {
                                result.success(null)
                            }
                        }

                        override fun onError(error: Error) {
                            result.error("500", "importAccount failed!", error.toString())
                        }
                    }
                    )
                } catch (e: Exception) {
                    result.error("500", "importAccount failed!", e.toString())
                }
            }
            "getProfile" -> {
                try {
                    UserManager.getInstance()
                        .getAccount(object : OnResultListener<ProfileResponse?> {
                            override fun onError(
                                e: Error
                            ) {
                                result.error("500", "getProfile failed!", e.toString())
                            }

                            override fun onSuccess(
                                profileResponse: ProfileResponse?
                            ) {
                                result.success(profileResponse?.toMap())
                            }
                        })
                } catch (e: Exception) {
                    result.error("500", "getProfile failed!", e.toString())
                }
            }
            "updateProfile" -> {
                try {
                    val firstName = call.argument<String>("firstName")!!
                    val lastName = call.argument<String>("lastName")!!
                    val city = call.argument<String?>("city")
                    val country = call.argument<String?>("country")
                    val disablePushNotifications =
                        call.argument<Boolean?>("disablePushNotifications")
                    val favoriteStoreID = call.argument<Int?>("favoriteStoreID")
                    val gender = call.argument<String?>("gender")
                    val houseNumber = call.argument<String?>("houseNumber")
                    val landlinePhone = call.argument<String?>("landlinePhone")
                    val language = call.argument<String?>("language")
                    val phoneNumber = call.argument<String?>("phoneNumber")
                    val street = call.argument<String?>("street")
                    val zipCode = call.argument<String?>("zipCode")
                    val birthDateSeconds = call.argument<Double?>("birthDateSeconds")
                    var birthday: Date? = null
                    if (birthDateSeconds != null) {
                        val stamp = Timestamp(birthDateSeconds.toLong() * 1000)
                        birthday = Date(stamp.time)
                    }

                    val request = UpdateProfileRequest()
                    request.firstName = firstName
                    request.lastName = lastName
                    request.birthDate = birthday
                    request.city = city
                    request.country = country
                    request.disablePushNotifications = disablePushNotifications?.toString()
                    request.favoriteStoreId = favoriteStoreID
                    request.gender = gender
                    request.houseNumber = houseNumber
                    request.landlinePhone = landlinePhone
                    request.language = language
                    request.phoneNumber = phoneNumber
                    request.street = street
                    request.zipCode = zipCode

                    UserManager.getInstance().updateAccount(request, object : OnCompleteListener {
                        override fun onSuccess() {
                            checkUser()
                            UserManager.getInstance()
                                .getAccount(object : OnResultListener<ProfileResponse?> {
                                    override fun onError(
                                        e: Error
                                    ) {
                                        result.error("500", "updateProfile failed!", e.toString())
                                    }

                                    override fun onSuccess(
                                        profileResponse: ProfileResponse?
                                    ) {
                                        result.success(profileResponse?.toMap())
                                    }
                                })
                        }

                        override fun onError(error: Error) {
                            result.error("500", "updateProfile failed!", error.toString())
                        }
                    })
                } catch (e: Exception) {
                    result.error("500", "updateProfile failed!", e.toString())
                }
            }
            "logout" -> {
                UserManager.getInstance().logout()
                result.success(true)
                checkUser()
            }
            "getOAuthContext" -> {
                val userId = checkUser()
                if (userId != null) {
                    result.success(mapOf("userId" to userId))
                } else {
                    result.success(null)
                }
            }
            "updateHostname" -> {
                try {
                    val hostname = call.argument<String>("hostname")
                    BeaconsmindSdk.updateSuiteUri(hostname)
                } catch (e: Exception) {
                    result.error("500", "updateHostname failed!", e.toString())
                }
            }
            "startListeningBeacons" -> {
                BeaconListenerService.getInstance().startListeningBeacons()
            }
            "stopListeningBeacons" -> {
                BeaconListenerService.getInstance().stopListeningBeacons()
            }
            "getBeaconsSummary" -> {
                val beacons = BeaconListenerService.getInstance().beaconsSummary
                val gson = Gson()
                val json =
                    gson.toJson(beacons.map {
                        mapOf(
                            "uuid" to it.uuid,
                            "major" to it.major,
                            "minor" to it.minor,
                            "name" to it.name,
                            "store" to it.store
                        )
                    })
                result.success(json)
            }
            "requestPermissions" -> {
                mActivity!!.requestPermissions(
                    arrayOf(
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.INTERNET,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                        Manifest.permission.FOREGROUND_SERVICE,
                        Manifest.permission.RECEIVE_BOOT_COMPLETED,
                        Manifest.permission.BLUETOOTH_SCAN
                    ),
                    31
                )
                result.success(true)
            }
            "registerDeviceToken" -> {
                try {
                    val token = call.argument<String>("deviceToken")!!
                    UserManager.getInstance().updateDeviceInfo(token)
                    result.success(true)
                } catch (e: Exception) {
                    result.error("500", "registerDeviceToken failed!", e.toString())
                }
            }
            "markOfferAsRead" -> {
                try {
                    val offerId = call.argument<Int>("offerId")!!
                    OffersManager.getInstance().markOfferAsRead(offerId)
                    result.success(true)
                } catch (e: Exception) {
                    result.error("500", "markOfferAsRead failed!", e.toString())
                }
            }
            "markOfferAsReceived" -> {
                try {
                    val offerId = call.argument<Int>("offerId")!!
                    OffersManager.getInstance().markOfferAsReceived(offerId)
                    result.success(true)
                } catch (e: Exception) {
                    result.error("500", "markOfferAsReceived failed!", e.toString())
                }
            }
            "markOfferAsRedeemed" -> {
                try {
                    val offerId = call.argument<Int>("offerId")!!
                    OffersManager.getInstance()
                        .markOfferRedeemed(offerId, object : OnResultListener<Void> {
                            override fun onError(error: Error) {
                                result.error("500", "markOfferAsRedeemed failed!", error.toString())
                            }

                            override fun onSuccess(
                                response: Void?
                            ) {
                                result.success(true)
                            }
                        })
                } catch (e: Exception) {
                    result.error("500", "markOfferAsRedeemed failed!", e.toString())
                }
            }
            "loadOffers" -> {
                try {
                    OffersManager.getInstance()
                        .loadOffers(object : OnResultListener<List<OfferResponse>> {
                            override fun onError(error: Error) {
                                result.error("500", "loadOffers failed!", error.toString())
                            }

                            override fun onSuccess(
                                response: List<OfferResponse>
                            ) {
                                val gson = Gson()
                                val json = gson.toJson(response)
                                result.success(json)
                            }
                        })
                } catch (e: Exception) {
                    result.error("500", "loadOffers failed!", e.toString())
                }
            }
            "loadOffer" -> {
                try {
                    val offerId = call.argument<Int>("offerId")!!
                    OffersManager.getInstance()
                        .loadOffer(offerId, object : OnResultListener<OfferResponse> {
                            override fun onError(error: Error) {
                                result.error("500", "loadOffer failed!", error.toString())
                            }

                            override fun onSuccess(
                                response: OfferResponse
                            ) {
                                val gson = Gson()
                                val json = gson.toJson(response)
                                result.success(json)
                            }
                        })
                } catch (e: Exception) {
                    result.error("500", "loadOffer failed!", e.toString())
                }
            }
            else -> {
                result.notImplemented()
            }
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        methods.setMethodCallHandler(null)
    }

    //
    // EventChannel.StreamHandler
    //
    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        if (eventsSink == null) {
            eventsSink = events
        }
        checkUser()
    }

    override fun onCancel(arguments: Any?) {
        eventsSink = null
    }
    //
    // End EventChannel.StreamHandler
    //

    //
    // ActivityAware
    //
    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        mActivity = binding.activity
        mApplication = binding.activity.application
    }

    override fun onDetachedFromActivityForConfigChanges() {
        mActivity = null
        mApplication = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        mActivity = binding.activity
        mApplication = binding.activity.application
    }

    override fun onDetachedFromActivity() {
        mActivity = null
        mApplication = null
    }
    //
    // End ActivityAware
    //

    private fun checkUser(): String? {
        if (eventsSink == null || mActivity == null) {
            return null
        }
        val userManager = UserManager.getInstance()
        val accessToken = userManager.accessToken
        return if (accessToken != null) {
            mActivity!!.runOnUiThread {
                eventsSink!!.success(mapOf("userId" to accessToken))
            }
            accessToken
        } else {
            mActivity!!.runOnUiThread {
                eventsSink!!.success(null)
            }
            null
        }
    }
}

@SuppressLint("SimpleDateFormat")
fun ProfileResponse.toMap(): Map<String, Any?> {
    val dateFormatter: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ")

    return mapOf(
        "disablePushNotifications" to disablePushNotifications,
        "firstName" to firstName,
        "joinDate" to dateFormatter.format(joinDate),
        "lastName" to lastName,
        "newsLetterSubscription" to newsLetterSubscription,
        "birthDate" to (birthDate?.let { dateFormatter.format(birthDate!!) }),
        "city" to city,
        "claims" to claims,
        "clubId" to clubId,
        "country" to country,
        "favoriteStore" to favoriteStore,
        "favoriteStoreId" to favoriteStoreId,
        "fullName" to fullName,
        "gender" to gender,
        "houseNumber" to houseNumber,
        "id" to id,
        "landlinePhone" to landlinePhone,
        "language" to language,
        "phoneNumber" to phoneNumber,
        "roles" to roles,
        "street" to street,
        "url" to url,
        "userName" to userName,
        "zipCode" to zipCode
    )
}
